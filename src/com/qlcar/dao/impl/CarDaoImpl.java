package com.qlcar.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.qlcar.constant.Common;
import com.qlcar.dao.CarDao;
import com.qlcar.dto.Car;
import com.qlcar.file.FileCar;

public class CarDaoImpl implements CarDao {

	private static List<Car> listCar = new ArrayList<Car>();

	@Override
	public void addCar(Car car) {
		// TODO Auto-generated method stub
		System.out.println("Nhập mã xe: ");
		int id = new Scanner(System.in).nextInt();
		System.out.println("Nhập tên xe: ");
		String name = new Scanner(System.in).nextLine();
		System.out.println("Nhập hãng xe: ");
		String companyCar = new Scanner(System.in).nextLine();
		System.out.println("Nhập năm sản xuất: ");
		int yearOfManufacture = new Scanner(System.in).nextInt();
		System.out.println("Nhập màu xe: ");
		String colorCar = new Scanner(System.in).nextLine();
		System.out.println("Nhập giá xe: ");
		int price = new Scanner(System.in).nextInt();
		car = new Car(id, name, companyCar, yearOfManufacture, colorCar, price);
		if (listCar.isEmpty()) {
			listCar.add(car);
			System.out.println("Thêm thành công!");
		} else {
			boolean flag = false;
			for (Car carTemp : listCar) {
				if (carTemp.getId() == id) {
					flag = true;
					break;
				}
			}
			if (flag == true) {
				System.out.println("lỗi trùng id xe");
			} else {
				listCar.add(car);
				System.out.println("Thêm thành công!");
			}

		}

	}

	@Override
	public List<Car> getlist() {
		for (Car car : listCar) {
			System.out.println(car.toString());
		}
		return listCar;
	}

	@Override
	public boolean getCarById(int id) {
		for (Car car : listCar) {
			if (car.getId() == id) {
				System.out.println(car.toString());
				return true;
			}

		}
		System.out.println("ko có mã xe:" + id);
		return false;
	}

	@Override
	public boolean updateCar(Car car) {
		System.out.println("Nhập id xe bạn muốn sửa: ");
		int id = new Scanner(System.in).nextInt();
		for (Car carTemp : listCar) {
			if (carTemp.getId() == id) {
				System.out.println("Nhập tên xe: ");
				String name = new Scanner(System.in).nextLine();
				carTemp.setName(name);
				System.out.println("Nhập hãng xe: ");
				String companyCar = new Scanner(System.in).nextLine();
				carTemp.setCompany(companyCar);
				System.out.println("Nhập năm sản xuất: ");
				int yearOfManufacture = new Scanner(System.in).nextInt();
				carTemp.setYearOfManufacture(yearOfManufacture);
				System.out.println("Nhập màu xe: ");
				String colorCar = new Scanner(System.in).nextLine();
				carTemp.setColor(colorCar);
				System.out.println("Nhập giá xe: ");
				int price = new Scanner(System.in).nextInt();
				carTemp.setPrice(price);
				return true;
			}
		}
		System.out.println("không tồn tại mã: " + id + " trong bảng ");
		return false;
	}

	@Override
	public boolean deleteCar(Car car) {
		System.out.println("Nhập id xe bạn muốn xóa: ");
		int id = new Scanner(System.in).nextInt();
		for (Car carTemp : listCar) {
			if (carTemp.getId() == id) {
				listCar.remove(carTemp);
				return true;
			}
		}
		System.out.println("không tồn tại mã: " + id + " trong bảng ");

		return false;
	}

	@Override
	public List<Car> searchCar(Car car, String name) {
		// TODO Auto-generated method stub
		int count = 0;
		for (Car carTemp : listCar) {

			if (carTemp.getName().startsWith(name) || carTemp.getName().equalsIgnoreCase(name)) {
				System.out.println(carTemp.toString());
				count = 1;
			}
		}
		if (count == 0) {
			System.out.println("ko tìm thấy xe nào có tên:" + name);
		}

		return listCar;
	}

	@Override
	public List<Car> getlistCarPriceMax() {
		List<Car> listTemp = new ArrayList<Car>();
		int priceMax = 0;

		if (listCar.isEmpty())
			System.out.println("Không có bản ghi nào");
		else {
			for (Car car : listCar) {
				if (car.getPrice() >= priceMax) {
					listTemp.clear();
					priceMax = car.getPrice();
					listTemp.add(car);
				}
			}
		}
		for (Car car : listTemp) {
			System.out.println(car.toString());
		}

		return listTemp;
	}

	@Override
	public boolean saveFile() {
		FileCar.saveFile(listCar, Common.PATH_FILE);
		return false;
	}

	@Override
	public boolean readFile() {
		// TODO Auto-generated method stub
		listCar = FileCar.readFile(Common.PATH_FILE);
		System.out.println(listCar);
		return false;
	}

}
