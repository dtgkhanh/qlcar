package com.qlcar.dao;

import java.util.List;

import com.qlcar.dto.Car;

public interface CarDao {

	void addCar(Car car);

	List<Car> getlist();

	boolean getCarById(int id);

	boolean updateCar(Car car);

	boolean deleteCar(Car car);

	List<Car> searchCar(Car car, String ten);

	List<Car> getlistCarPriceMax();

	boolean saveFile();

	boolean readFile();
}
