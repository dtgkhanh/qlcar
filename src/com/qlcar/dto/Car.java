package com.qlcar.dto;

import java.io.Serializable;

public class Car implements Serializable {

	private static final long serialVersionUID = 8370135776838513981L;

	private int id;
	private String name;
	private String company;
	private int yearOfManufacture;
	private String color;
	private int price;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public int getYearOfManufacture() {
		return yearOfManufacture;
	}

	public void setYearOfManufacture(int yearOfManufacture) {
		this.yearOfManufacture = yearOfManufacture;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Car() {
		super();
	}

	public Car(int id, String name, String company, int yearOfManufacture, String color, int price) {
		super();
		this.id = id;
		this.name = name;
		this.company = company;
		this.yearOfManufacture = yearOfManufacture;
		this.color = color;
		this.price = price;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		String str = "Car [id=" + id + ", Tên=" + name + ", Hãng =" + company + ", năm sản xuất =" + yearOfManufacture
				+ ", Màu sắc=" + color + ", Giá=" + price + "] ";
		sb.append(str).append(System.getProperty("line.separator"));
		return sb.toString();
	}

}
