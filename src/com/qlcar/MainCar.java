package com.qlcar;

import java.util.Scanner;

import com.qlcar.dao.CarDao;
import com.qlcar.dao.impl.CarDaoImpl;
import com.qlcar.dto.Car;

public class MainCar {

	public static CarDao carDao = new CarDaoImpl();
	public static Car car = new Car();

	private static void menu() {
		System.out.println("1. Thêm xe: ");
		System.out.println("2. Hiển thị danh sách xe: ");
		System.out.println("3. Hiển thị danh sách xe theo id: ");
		System.out.println("4. Cập nhật xe theo id: ");
		System.out.println("5. Xóa xe theo id: ");
		System.out.println("6. Lưu thông tin xe vào file ");
		System.out.println("7. Đọc thông tin xe từ file đã lưu ");
		System.out.println("8. Tìm kiếm xe theo tên: ");
		System.out.println("9. Hiển thị xe có giá cao nhất: ");
		System.out.println("0. Thoát chương trình ");
		System.out.println("Bạn chọn gì: ");
		int chon = new Scanner(System.in).nextInt();
		switch (chon) {

		case 1: {
			carDao.addCar(car);
			break;
		}
		case 2: {
			carDao.getlist();
			break;
		}
		case 3: {
			System.out.println("Nhập id xe bạn muốn hiển thị: ");
			int id = new Scanner(System.in).nextInt();
			carDao.getCarById(id);
			break;
		}
		case 4: {

			carDao.updateCar(car);
			break;
		}
		case 5: {
			carDao.deleteCar(car);
			break;
		}
		case 6: {
			carDao.saveFile();
			break;
		}
		case 7: {
			carDao.readFile();
			break;
		}
		case 8: {
			System.out.println("Nhập tên xe muốn tìm : ");
			String ten = new Scanner(System.in).nextLine();
			carDao.searchCar(car, ten);
			break;
		}
		case 9: {

			carDao.getlistCarPriceMax();
			break;
		}
		case 0: {
			System.out.println("Thoát chương trình...");
			System.exit(0);
			break;
		}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		while (true) {
			menu();
		}

	}
}